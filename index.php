<?php

/**
 * Bot Limpeza de Chat
 * @author @fotavares
 */
include '../Include/Telegram.php';
include 'config.php';

$telegram = new Telegram($bot_token);
// Get all the new updates and set the new correct update_id
$req = $telegram->getUpdates();

for ($i = 0; $i < $telegram->UpdateCount(); $i++) {
    // You NEED to call serveUpdate before accessing the values of message in Telegram Class
    $telegram->serveUpdate($i);
    $text = $telegram->Text();
    $chat_id = $telegram->ChatID();
    $message_id = $telegram->MessageID();
    
    $data = $telegram->getData();
    $user = $data['message']['from']['username'];
    $date = $data['message']['date'];

    if($i == 0)
    {
      
    	$content = ['chat_id' => $chat_id, 'text' => "Removendo Mensagens..."];
        $telegram->sendMessage($content);
    }
    
        
	if(!isset($data['edited_message']))
	{
	       $content = ['chat_id' => $chat_id, 'message_id' => $message_id];
		$telegram->deleteMessage($content);
	}
	else
	{
	 $content = ['chat_id' => $data['edited_message']['chat']['id'], 'message_id' => $data['edited_message']['message_id']];
		$telegram->deleteMessage($content);
	}
	    
}

?>
